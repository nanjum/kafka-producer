package org.kafka.sample.utils;

/**
 * Created by nishat on 11/12/18.
 */
public interface IKafkaConstants {
    String KAFKA_BROKERS = "localhost:9092,localhost:9093,localhost:9094";
    int PARTITION_COUNT = 13;
    Integer MESSAGE_COUNT = 1000;
    String CLIENT_ID = "producer_1";
    String TOPIC_NAME = "my-test-topic";
    String GROUP_ID_CONFIG = "myGroup";
    Integer MAX_NO_MESSAGE_FOUND_COUNT = 100;
    String OFFSET_RESET_LATEST = "latest";
    String OFFSET_RESET_EARLIER = "earliest";
    Integer MAX_POLL_RECORDS = 1;
}
