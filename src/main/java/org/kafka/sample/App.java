package org.kafka.sample;

import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.kafka.sample.messages.Message;
import org.kafka.sample.producers.ProducerCreator;
import org.kafka.sample.utils.IKafkaConstants;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        runProducer();
    }

    private static void runProducer() {
        Producer<Long, Message> producer = ProducerCreator.createProducer();
        for (int index = 0; index < IKafkaConstants.MESSAGE_COUNT; index++) {
            Message message = createNessage(index);
            ProducerRecord<Long, Message> record = new ProducerRecord<>(IKafkaConstants.TOPIC_NAME, message);
            try {
                RecordMetadata metadata = producer.send(record).get();

                System.out.println("Record sent with key " + index + " to partition " + metadata.partition()

                        + " with offset " + metadata.offset());
            } catch (ExecutionException e) {
                System.out.println("Error in sending record");
                System.out.println(e);
            } catch (InterruptedException e) {
                System.out.println("Error in sending record");
                System.out.println(e);
            }
        }
        close(producer);
    }

    private static Message createNessage(int index) {
        Message message = new Message();
        message.setRegNo(StringUtils.join(new Object[]{"Reg_no", index}, "_"));
        message.setRegNoBangla(StringUtils.join(new Object[]{"Reg_no_Bangla", index}, "_"));
        message.setPersoStatus(index);
        return message;
    }

    public static boolean close(Producer producer) {
        try {
            producer.flush();
            producer.close();
        } catch (Exception e) {
            System.out.println("There was an error when closing kafka producer." + e);
            return false;
        }
        System.out.println("Kafka producer closed.");
        return true;
    }
}
