package org.kafka.sample.common;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;
import org.kafka.sample.utils.IKafkaConstants;

import java.util.Map;

/**
 * Created by nishat on 11/12/18.
 */
public class CustomPartitioner implements Partitioner {

    @Override
    public void configure(Map<String, ?> configs) {
    }

    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
        Integer keyInt = Integer.parseInt(key.toString());
        return keyInt % IKafkaConstants.PARTITION_COUNT;
    }

    @Override
    public void close() {
    }
}
