package org.kafka.sample.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Serializer;
import org.kafka.sample.messages.Message;

import java.util.Map;

/**
 * Created by nishat on 11/12/18.
 */
public class MessageSerializer implements Serializer<Message> {
    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

    @Override
    public byte[] serialize(String topic, Message message) {
        byte[] retVal = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            retVal = objectMapper.writeValueAsString(message).getBytes();
        } catch (Exception exception) {
            System.out.println("Error in serializing object" + message);
        }
        return retVal;
    }

    @Override
    public void close() {

    }
}
