package org.kafka.sample.messages;

/**
 * Created by nishat on 11/12/18.
 */
public class Message {
    private String regNo;
    private String regNoBangla;
    private int persoStatus;

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getRegNoBangla() {
        return regNoBangla;
    }

    public void setRegNoBangla(String regNoBangla) {
        this.regNoBangla = regNoBangla;
    }

    public int getPersoStatus() {
        return persoStatus;
    }

    public void setPersoStatus(int persoStatus) {
        this.persoStatus = persoStatus;
    }
}
